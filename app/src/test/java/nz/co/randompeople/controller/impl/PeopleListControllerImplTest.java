package nz.co.randompeople.controller.impl;

import nz.co.randompeople.controller.PeopleListController;
import nz.co.randompeople.model.Id;
import nz.co.randompeople.model.People;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PeopleListControllerImplTest {
    private PeopleListController peopleListController;
    private Id id1;
    @Before
    public void setUp() throws Exception {
        peopleListController = new PeopleListControllerImpl(null);

        List<People> peopleList = new ArrayList<>();
        People people1 = new People();
        id1 = new Id().withName("foo").withValue("bar");
        people1.setId(id1);
        People people2 = new People();
        people2.setId(new Id().withName("foo1").withValue("bar1"));
        People people3 = new People();
        people3.setId(new Id().withName("foo2").withValue("bar2"));
        peopleList.add(people1);
        peopleList.add(people2);
        peopleList.add(people3);
        peopleListController.addPeopleList(peopleList);
    }

    @Test(expected= IllegalArgumentException.class)
    public void addNullPeople() {
        peopleListController.addPeople(null);
    }

    @Test(expected= IllegalArgumentException.class)
    public void addNullPeopleList() {
        peopleListController.addPeopleList(null);
    }

    @Test
    public void addPeople() throws Exception {
        People people = new People();
        people.setId(new Id().withName("foo3").withValue("bar3"));
        peopleListController.addPeople(people);

        assertEquals(4, peopleListController.getPeopleList().size());
        assertEquals(4, peopleListController.getPeopleMap().size());
    }

    @Test
    public void addPeopleList() throws Exception {
        peopleListController.clearPeopleList();
        List<People> peopleList = new ArrayList<>();
        People people1 = new People();
        people1.setId(new Id().withName("foo").withValue("bar"));
        People people2 = new People();
        people2.setId(new Id().withName("foo1").withValue("bar1"));
        People people3 = new People();
        people3.setId(new Id().withName("foo2").withValue("bar2"));
        peopleList.add(people1);
        peopleList.add(people2);
        peopleList.add(people3);
        peopleListController.addPeopleList(peopleList);

        assertTrue(peopleListController.getPeopleList().size() == 3);
        assertTrue(peopleListController.getPeopleMap().size() == 3);
    }

    @Test
    public void getPeople() throws Exception {
        assertTrue(peopleListController.getPeople(id1) != null);
        assertTrue(peopleListController.getPeople(new Id()) == null);
    }

    @Test
    public void clearPeoples() throws Exception {
        peopleListController.clearPeopleList();
        assertTrue(peopleListController.getPeopleList().size() == 0);
        assertTrue(peopleListController.getPeopleMap().size() == 0);
    }
}