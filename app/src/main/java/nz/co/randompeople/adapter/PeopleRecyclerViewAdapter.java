package nz.co.randompeople.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import nz.co.randompeople.R;
import nz.co.randompeople.controller.PeopleListController;
import nz.co.randompeople.fragment.PeopleListFragment;
import nz.co.randompeople.model.Id;
import nz.co.randompeople.model.People;

import java.util.List;
import java.util.Map;

/**
 * {@link PeopleRecyclerViewAdapter} that can display a {@link nz.co.randompeople.model.People} and makes a call to the
 * specified {@link nz.co.randompeople.fragment.PeopleListFragment.OnListFragmentInteractionListener}.
 */
public class PeopleRecyclerViewAdapter extends RecyclerView.Adapter<PeopleRecyclerViewAdapter.ViewHolder> {

    private final Context mCtx;
    private final List<People> mPeopleList;
    private final Map<Id, People> mPeopleMap;
    private final PeopleListFragment.OnListFragmentInteractionListener mListener;
    private final PeopleListController mPeopleListController;

    /**
     * Instantiates a new People List recycler view adapter.
     *
     * @param context             the context
     * @param listener            the listener
     * @param peopleListController the people list controller
     */
    public PeopleRecyclerViewAdapter(Context context, PeopleListFragment.OnListFragmentInteractionListener listener, PeopleListController peopleListController) {
        mCtx = context;
        mPeopleList = peopleListController.getPeopleList();
        mPeopleMap = peopleListController.getPeopleMap();
        mListener = listener;
        mPeopleListController = peopleListController;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_people, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mPeopleList.get(position);
        holder.mNameView.setText(holder.mItem.getName().toString());
        holder.mGenderView.setText(holder.mItem.getGender());
        holder.mDobView.setText(holder.mItem.getDob().getFormattedDate());

        holder.mPhotoProgressBar.setVisibility(View.VISIBLE);
        //Load thumbnail
        Glide.with(mCtx)
                .load(holder.mItem.getPicture().getThumbnail())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.mPhotoProgressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        //hide progressbar and show imageView when resource is ready
                        holder.mPhotoProgressBar.setVisibility(View.GONE);
                        holder.mPhotoImageView.setVisibility(View.VISIBLE);
                        return false;
                    }
                })
                .crossFade(1000)
                .into(holder.mPhotoImageView);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPeopleList.size();
    }

    /**
     * The type View holder.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        public final TextView mNameView;

        public final TextView mGenderView;

        public final TextView mDobView;

        public final ImageView mPhotoImageView;

        public final ProgressBar mPhotoProgressBar;

        public People mItem;

        /**
         * Instantiates a new View holder.
         *
         * @param view the view
         */
        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = (TextView) view.findViewById(R.id.name);
            mGenderView = (TextView) view.findViewById(R.id.gender);
            mDobView = (TextView) view.findViewById(R.id.dob);
            mPhotoImageView = (ImageView) view.findViewById(R.id.photoThumbnail);
            mPhotoProgressBar = (ProgressBar) view.findViewById(R.id.photoProgressBar);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
