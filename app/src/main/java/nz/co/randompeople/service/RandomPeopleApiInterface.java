package nz.co.randompeople.service;

import nz.co.randompeople.model.PeopleListResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by yhuang on 12/07/2018.
 */
public interface RandomPeopleApiInterface {
    @GET("api/")
    Call<PeopleListResponse> getPeopleList(@Query("seed") String seed, @Query("results") Integer results, @Query("page") Integer page);

}
