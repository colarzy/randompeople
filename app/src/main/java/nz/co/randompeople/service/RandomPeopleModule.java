package nz.co.randompeople.service;

import android.content.Context;
import com.google.gson.Gson;
import dagger.Module;
import dagger.Provides;
import nz.co.randompeople.controller.PeopleListController;
import nz.co.randompeople.controller.impl.PeopleListControllerImpl;
import nz.co.randompeople.util.Utils;
import okhttp3.*;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import javax.inject.Singleton;
import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by yhuang on 12/07/2018.
 */

@Module
public class RandomPeopleModule {
    //https://randomuser.me/api/?seed=foobar&results=2&page=3
    public static final String SEED = "foobar";
    public static final int RESULTS = 20;
    public static final String BASE_URL = "https://randomuser.me/";
    public static final String HEADER_CACHE_CONTROL = "Cache-Control";
    public static final String HEADER_PRAGMA = "Pragma";
    private final Context mContext;

    public RandomPeopleModule(Context context){
        this.mContext = context;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return mContext;
    }

    @Provides
    @Singleton
     Retrofit provideRetrofit() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .addInterceptor(provideOfflineCacheInterceptor())
                .addNetworkInterceptor(provideCacheInterceptor())
                .cache(provideCache());

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .client(httpClient.build())
                .build();
    }

    @Provides
    @Singleton
    RandomPeopleApiInterface provideRandomPeopleApiInterface(Retrofit retrofit){
        return retrofit.create(RandomPeopleApiInterface.class);
    }

    @Provides
    @Singleton
    PeopleListController providePeopleListController(Context context, RandomPeopleApiInterface randomPeopleApiInterface){
        return new PeopleListControllerImpl(randomPeopleApiInterface);
    }

    private Cache provideCache() {
        Cache cache = null;

        try {
            cache = new Cache(new File(mContext.getCacheDir(), "http-cache"),
                    10 * 1024 * 1024); // 10 MB
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cache;
    }

    private Interceptor provideCacheInterceptor() {
        return chain -> {
            Response response = chain.proceed(chain.request());

            CacheControl cacheControl;

            if (Utils.isNetworkAvailable(mContext)) {
                cacheControl = new CacheControl.Builder()
                        .maxAge(0, TimeUnit.SECONDS)
                        .build();
            } else {
                cacheControl = new CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build();
            }

            return response.newBuilder()
                    .removeHeader(HEADER_PRAGMA)
                    .removeHeader(HEADER_CACHE_CONTROL)
                    .header(HEADER_CACHE_CONTROL, cacheControl.toString())
                    .build();

        };
    }

    private Interceptor provideOfflineCacheInterceptor() {
        return chain -> {
            Request request = chain.request();

            if (!Utils.isNetworkAvailable(mContext)) {
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build();

                request = request.newBuilder()
                        .removeHeader(HEADER_PRAGMA)
                        .removeHeader(HEADER_CACHE_CONTROL)
                        .cacheControl(cacheControl)
                        .build();
            }

            return chain.proceed(request);
        };
    }

}
