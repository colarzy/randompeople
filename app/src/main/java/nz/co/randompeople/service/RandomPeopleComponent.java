package nz.co.randompeople.service;


import dagger.Component;
import nz.co.randompeople.activity.PeopleListActivity;
import nz.co.randompeople.activity.PeopleDetailActivity;
import nz.co.randompeople.fragment.PeopleListFragment;

import javax.inject.Singleton;

/**
 * Created by yhuang on 12/07/2018.
 */
@Singleton
@Component(modules={RandomPeopleModule.class})
public interface RandomPeopleComponent {
    void inject(PeopleListActivity activity);
    void inject(PeopleListFragment listFragment);
//    void inject(PhotoListFragment photoListFragment);
    void inject(PeopleDetailActivity peopleDetailActivity);
}
