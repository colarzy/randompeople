package nz.co.randompeople.service;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by yhuang on 12/07/2018.
 */
@Qualifier
@Retention(RUNTIME)
public @interface ForApplication {
}
