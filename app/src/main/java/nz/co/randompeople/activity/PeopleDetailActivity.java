package nz.co.randompeople.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import nz.co.randompeople.R;
import nz.co.randompeople.model.People;


/**
 * The type Photo detail activity.
 */
public class PeopleDetailActivity extends AppCompatActivity {

    private ImageView mImageView;
    private ProgressBar mPhotoProgressBar;
    private People mPeople;

    private static final String TAG = PeopleDetailActivity.class.getSimpleName();
    /**
     * The constant PEOPLE.
     */
    public static final String PEOPLE = "PEOPLE";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);

        Bundle bundle = getIntent().getExtras();
        mPeople = bundle.getParcelable(PeopleDetailActivity.PEOPLE);
        setupUI();

        loadImage();
    }

    /**
     * Prepare UI
     */
    private void setupUI() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView titleTextView = findViewById(R.id.appTitle);
        titleTextView.setText(mPeople.getName().toString());
        mImageView = findViewById(R.id.photo);
        mPhotoProgressBar = findViewById(R.id.photoProgressBar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }
    }

    /**
     * Load the original image
     */
    private void loadImage(){
        Glide.with(this)
                .load(mPeople.getPicture().getLarge())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mPhotoProgressBar.setVisibility(View.GONE);
                        Log.d(TAG, "Glide onException: "+e.toString());
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mPhotoProgressBar.setVisibility(View.GONE);
                        mImageView.setVisibility(View.VISIBLE);
                        Log.d(TAG, "Glide onResourceReady ");
                        return false;
                    }
                })
                .crossFade(1000)
                .into(mImageView);
    }

    /**
     * Handle back button
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }
}
