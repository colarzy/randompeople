package nz.co.randompeople.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import nz.co.randompeople.MyApplication;
import nz.co.randompeople.R;
import nz.co.randompeople.fragment.PeopleListFragment;
import nz.co.randompeople.model.People;


public class PeopleListActivity extends AppCompatActivity implements PeopleListFragment.OnListFragmentInteractionListener {

    /**
     * The constant TAG.
     */
    private static final String TAG = PeopleListActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((MyApplication) getApplication()).getRandomPeopleComponent().inject(this);

        setupUI();
    }

    /**
     * Prepare UI
     */
    private void setupUI(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("");

        TextView title = (TextView) findViewById(R.id.appTitle);
        title.setText(R.string.app_name);
    }

    /**
     * Navigate to next screen
     */
    private void showPeopleDetail(People people){
        Intent intent = new Intent(this, PeopleDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(PeopleDetailActivity.PEOPLE, people);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * Handle onItemClick
     */
    @Override
    public void onListFragmentInteraction(People people) {
        showPeopleDetail(people);
    }
}
