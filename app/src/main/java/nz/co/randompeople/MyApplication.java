package nz.co.randompeople;

import android.app.Application;
import nz.co.randompeople.service.DaggerRandomPeopleComponent;
import nz.co.randompeople.service.RandomPeopleComponent;
import nz.co.randompeople.service.RandomPeopleModule;

/**
 * Created by yhuang on 12/07/2018.
 */
public class MyApplication extends Application {

    private RandomPeopleComponent mRandomPeopleComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        //Injection
        mRandomPeopleComponent = DaggerRandomPeopleComponent.builder()
                .randomPeopleModule(new RandomPeopleModule(this))
                .build();
    }

    public RandomPeopleComponent getRandomPeopleComponent(){
        return mRandomPeopleComponent;
    }
}
