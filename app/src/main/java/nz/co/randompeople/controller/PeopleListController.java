package nz.co.randompeople.controller;


import nz.co.randompeople.listener.PeopleListListener;
import nz.co.randompeople.model.Id;
import nz.co.randompeople.model.People;

import java.util.List;
import java.util.Map;

/**
 * Created by yhuang on 12/07/2018.
 */
public interface PeopleListController {

    void loadPeopleList(PeopleListListener peopleListListener, int page);

    void addPeople(People item);
    void addPeopleList(List<People> list);
    List<People> getPeopleList();
    Map<Id, People> getPeopleMap();
    People getPeople(Id id);
    void clearPeopleList();


}
