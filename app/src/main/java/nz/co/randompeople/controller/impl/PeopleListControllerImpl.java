package nz.co.randompeople.controller.impl;

import android.util.Log;
import nz.co.randompeople.controller.PeopleListController;
import nz.co.randompeople.listener.PeopleListListener;
import nz.co.randompeople.model.Id;
import nz.co.randompeople.model.People;
import nz.co.randompeople.model.PeopleListResponse;
import nz.co.randompeople.service.RandomPeopleApiInterface;
import nz.co.randompeople.service.RandomPeopleModule;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.*;

/**
 * The type People list controller.
 */
public class PeopleListControllerImpl implements PeopleListController {

    private static final String TAG = PeopleListControllerImpl.class.getSimpleName();
    private final RandomPeopleApiInterface mRandomPeopleApiInterface;

    /**
     * Instantiates a new People list controller.
     *
     * @param randomPeopleApiInterface the people api interface
     */
    public PeopleListControllerImpl(RandomPeopleApiInterface randomPeopleApiInterface) {
        this.mRandomPeopleApiInterface = randomPeopleApiInterface;
    }

    /**
     * An array of people.
     */
    private List<People> peopleList = new ArrayList<People>();

    /**
     * A map of people, by ID.
     */
    private Map<Id, People> peopleMap = new HashMap<Id, People>();


    public void addPeople(People item) {
        if (item == null)
            throw new IllegalArgumentException("People cannot be null.");
        peopleList.add(item);
        peopleMap.put(item.getId(), item);
    }

    public void addPeopleList(List<People> list) {
        if (list == null)
            throw new IllegalArgumentException("People list cannot be null.");
        clearPeopleList();
        for (People people : list) {
            addPeople(people);
        }
    }

    public List<People> getPeopleList() {
        return peopleList;
    }

    public Map<Id, People> getPeopleMap() {
        return peopleMap;
    }

    public People getPeople(Id id) {
        if (id == null)
            throw new IllegalArgumentException("People Id cannot be null.");
        return peopleMap.get(id);
    }

    public void clearPeopleList() {
        peopleList.clear();
        peopleMap.clear();
    }

    /**
     * Prepare the user Ids from the existing peopleList
     * and remove the duplicated ones
     *
     * @param peopleList           the peopleList
     */
    private Id[] getUserIdsForRequest(List<People> peopleList){
        if (peopleList == null)
            throw new IllegalArgumentException("Peoples cannot be null.");
        Set<Id> userIdSet = new HashSet<>();
        for (People people : peopleList) {
            userIdSet.add(people.getId());
        }
        return userIdSet.toArray(new Id[userIdSet.size()]);
    }

    @Override
    public void loadPeopleList(final PeopleListListener peopleListListener, int page) {
        Callback<PeopleListResponse> callback = new Callback<PeopleListResponse>() {
            @Override
            public void onResponse(Call<PeopleListResponse> call, Response<PeopleListResponse> response) {
                Log.d(TAG, "onResponse: " + response);
                addPeopleList(response.body().getPeopleList());
                if(peopleListListener != null){
                    peopleListListener.onLoadPeopleSuccess(response.body().getPeopleList());
                }
            }

            @Override
            public void onFailure(Call<PeopleListResponse> call, Throwable t) {
                Log.e(TAG, t.toString());
                if (peopleListListener != null)
                    peopleListListener.onFail(t.getMessage());
            }
        };
        Call<PeopleListResponse> call = mRandomPeopleApiInterface.getPeopleList(RandomPeopleModule.SEED, RandomPeopleModule.RESULTS, page);
        call.enqueue(callback);
    }

}
