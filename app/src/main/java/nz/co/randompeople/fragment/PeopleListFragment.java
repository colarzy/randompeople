package nz.co.randompeople.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import nz.co.randompeople.MyApplication;
import nz.co.randompeople.R;
import nz.co.randompeople.adapter.PeopleRecyclerViewAdapter;
import nz.co.randompeople.controller.PeopleListController;
import nz.co.randompeople.listener.PeopleListListener;
import nz.co.randompeople.model.People;

import javax.inject.Inject;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class PeopleListFragment extends Fragment implements PeopleListListener {

    private static final String TAG = PeopleListFragment.class.getSimpleName();
    private OnListFragmentInteractionListener mListener;
    private RecyclerView mRecyclerView;
    private int mCurrentPage = 0;

    /**
     * The people list controller.
     */
    @Inject
    PeopleListController mPeopleListController;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PeopleListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Injection
        ((MyApplication) getActivity().getApplication()).getRandomPeopleComponent().inject(this);

        requestPeopleList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_people_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            mRecyclerView = (RecyclerView) view;
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
            mRecyclerView.setLayoutManager(layoutManager);
            //add item separator
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                    ((LinearLayoutManager) layoutManager).getOrientation());
            mRecyclerView.addItemDecoration(dividerItemDecoration);

        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void requestPeopleList() {
        mPeopleListController.loadPeopleList(this, mCurrentPage);
    }

    private void showErrorMessage(String title, String message){
        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //If there is no internet connection quit the app
                        getActivity().finish();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void onFail(String message) {
        showErrorMessage("Error", message);
    }

    @Override
    public void onLoadPeopleSuccess(List<People> peopleList) {
        if(peopleList == null || peopleList.isEmpty()){
            showErrorMessage(getString(R.string.load_people_list_error_title), getString(R.string.load_people_list_error_message));
        }else{
            mRecyclerView.setAdapter(new PeopleRecyclerViewAdapter(getActivity(), mListener, mPeopleListController));
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        /**
         * On list fragment interaction.
         *
         * @param people the people
         */
        void onListFragmentInteraction(People people);
    }
}
