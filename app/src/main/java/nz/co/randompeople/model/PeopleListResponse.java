
package nz.co.randompeople.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PeopleListResponse implements Serializable, Parcelable
{

    @SerializedName("results")
    @Expose
    private List<People> peopleList = null;
    @SerializedName("info")
    @Expose
    private Info info;
    public final static Parcelable.Creator<PeopleListResponse> CREATOR = new Creator<PeopleListResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public PeopleListResponse createFromParcel(Parcel in) {
            return new PeopleListResponse(in);
        }

        public PeopleListResponse[] newArray(int size) {
            return (new PeopleListResponse[size]);
        }

    }
    ;
    private final static long serialVersionUID = -5832780900052501770L;

    protected PeopleListResponse(Parcel in) {
        in.readList(this.peopleList, (People.class.getClassLoader()));
        this.info = ((Info) in.readValue((Info.class.getClassLoader())));
    }

    public PeopleListResponse() {
    }

    public List<People> getPeopleList() {
        return peopleList;
    }

    public void setPeopleList(List<People> peopleList) {
        this.peopleList = peopleList;
    }

    public PeopleListResponse withPeopleList(List<People> people) {
        this.peopleList = people;
        return this;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public PeopleListResponse withInfo(Info info) {
        this.info = info;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(peopleList);
        dest.writeValue(info);
    }

    public int describeContents() {
        return  0;
    }

}
