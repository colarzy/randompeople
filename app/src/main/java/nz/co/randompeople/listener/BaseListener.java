package nz.co.randompeople.listener;

/**
 * Created by yhuang on 12/07/2018.
 */
public interface BaseListener {
    void onFail(String message);
}
