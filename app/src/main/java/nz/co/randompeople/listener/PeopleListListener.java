package nz.co.randompeople.listener;

import nz.co.randompeople.model.People;

import java.util.List;

/**
 * Created by yhuang on 12/07/2018.
 */
public interface PeopleListListener extends BaseListener {
    void onLoadPeopleSuccess(List<People> peopleList);
}
